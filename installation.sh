#!/usr/bin/env bash

# === Colors yay + pacman ===
sudo sed -i "36s#.*#Color#" /etc/pacman.conf

# == Intel Driver ==
sudo pacman -S libva-intel-driver --noconfirm # Requirment for HwAcc of Parsec btw

# == Basic dev ==
sudo pacman -S base-devel gdb --noconfirm

# === Yay ===
sudo pacman -S yay --noconfirm
yay -Y --devel --save # dev package update -> enabled

# Disable wayland
sudo sed -i "6s#.*#WaylandEnable=false#" /etc/gdm/custom.conf

# === Fish ===
sudo pacman -S fish bat exa micro --noconfirm
# Remove motd
fish -c "set -U fish_greeting"
# Reversed search
curl -sL https://git.io/fisher | fish -c "source && fisher install jorgebucaran/fisher"
fish -c "fisher install jethrokuan/fzf"
# SSH
rm "$HOME"/.config/fish/functions/fish_ssh_agent.fish 2> /dev/null
wget -q https://gitlab.com/kyb/fish_ssh_agent/raw/master/functions/fish_ssh_agent.fish -P "$HOME"/.config/fish/functions/
mkdir "$HOME"/.ssh
# Custom config.fish
rm "$HOME"/.config/fish/config.fish 2> /dev/null
wget -q https://git.kennel.ml/Anri/manjaroConfiguration/raw/branch/main/.config/fish/config.fish -P "$HOME"/.config/fish/
# Custom prompt shell
rm "$HOME"/.config/fish/functions/fish_prompt.fish 2> /dev/null
wget -q https://git.kennel.ml/Anri/manjaroConfiguration/raw/branch/main/.config/fish/functions/fish_prompt.fish -P "$HOME"/.config/fish/functions/
fish -c "set -U fish_color_normal normal"
fish -c "set -U fish_color_command 99cc99"
fish -c "set -U fish_color_quote ffcc66"
fish -c "set -U fish_color_redirection d3d0c8"
fish -c "set -U fish_color_end cc99cc"
fish -c "set -U fish_color_error f2777a"
fish -c "set -U fish_color_param d3d0c8"
fish -c "set -U fish_color_comment ffcc66"
fish -c "set -U fish_color_match 6699cc"
fish -c "set -U fish_color_selection white --bold --background=brblack"
fish -c "set -U fish_color_search_match bryellow --background=brblack"
fish -c "set -U fish_color_history_current --bold"
fish -c "set -U fish_color_operator 6699cc"
fish -c "set -U fish_color_escape 66cccc"
fish -c "set -U fish_color_cwd green"
fish -c "set -U fish_color_cwd_root red"
fish -c "set -U fish_color_valid_path --underline"
fish -c "set -U fish_color_autosuggestion 747369"
fish -c "set -U fish_color_user brgreen"
fish -c "set -U fish_color_host normal"
fish -c "set -U fish_color_cancel -r"
fish -c "set -U fish_pager_color_completion normal"
fish -c "set -U fish_pager_color_description B3A06D yellow"
fish -c "set -U fish_pager_color_prefix normal --bold --underline"
fish -c "set -U fish_pager_color_progress brwhite --background=cyan"
# Add aliases
fish -c "abbr ls 'exa -glh'"
fish -c "abbr cp 'cp -rv'"
fish -c "abbr rm 'rm -rf'"
fish -c "abbr gcc 'clear; gcc -Wall -Wextra -fanalyzer main.c && ./a.out; rm a.out 2> /dev/null'"
fish -c "abbr g++ 'clear; g++ -Wall -Wextra -fanalyzer -std=c++17 main.cpp && ./a.out; rm a.out 2> /dev/null'"
fish -c "abbr make 'clear; make && ./main; make clean 2> /dev/null'"
fish -c "abbr c 'command'"
fish -c "abbr activate 'source bin/activate.fish'"
fish -c "abbr vs 'code .'"
fish -c "abbr untgz 'tar -xvzf'"
fish -c "abbr - 'cd -'"
fish -c "abbr cat 'bat'"
fish -c "abbr nano 'micro'"
fish -c "abbr bigupdate 'yay -Syu --noconfirm && tldr --update && echo \'y\' | sdk upgrade java'"
fish -c "abbr d 'nautilus . -w &> /dev/null & disown'"
fish -c "abbr spotify 'LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify &> /dev/null & disown'"
# Remove fish icon
if [ -f /usr/share/applications/fish.desktop ]
then
    sudo mv /usr/share/applications/fish.desktop /usr/share/applications/fish.desktop.bak
    echo -e "\nFish icon removed!"
fi
# Fish by default in terminal app
profil=$(gsettings get org.gnome.Terminal.ProfilesList list)
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "use-custom-command" "true"
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "custom-command" "fish"
# Default colors
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "use-theme-colors" "false"
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "foreground-color" "rgb(211,208,200)"
gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "background-color" "rgb(45,45,45)"

# == Brave instead of Firefox ==
sudo pacman -Rsn firefox firefox-gnome-theme-maia --noconfirm # firefox removal
yay -S brave-bin --noconfirm

# == GL4D ==
sudo pacman -S sdl2_image pkg-config automake make autoconf --noconfirm
# Download and build
git clone https://github.com/noalien/GL4Dummies.git
cd GL4Dummies || exit
make -f Makefile.autotools
./configure
make
# Install it
sudo make install
# Remove folder
cd ..
rm -rf GL4Dummies
# Fix for shared libraries (https://stackoverflow.com/a/9395355)
sudo ldconfig
# Add to path
if [ -f "$HOME"/.bashrc ] ; then
    echo -e "\n# GL4Dummies\nexport PATH=/usr/local/bin:\$PATH\nexport LD_LIBRARY_PATH=/usr/local/lib:\$LD_LIBRARY_PATH\n" >> "$HOME"/.bashrc
fi
if [ -f "$HOME"/.zshrc ] ; then
    echo -e "\n# GL4Dummies\nexport PATH=/usr/local/bin:\$PATH\nexport LD_LIBRARY_PATH=/usr/local/lib:\$LD_LIBRARY_PATH\n" >> "$HOME"/.zshrc
fi
if [ -f "$HOME"/.config/fish/config.fish ] ; then
    echo -e "\n# GL4Dummies\nset -gx LD_LIBRARY_PATH /usr/local/lib \$LD_LIBRARY_PATH\n" >> "$HOME"/.config/fish/config.fish
fi

# == Latex ==
sudo pacman -S texlive-most python-pygments --noconfirm

# == Python stuff ==
sudo pacman -S python-virtualenv tk --noconfirm

# == Sign commits ==
git config --global commit.gpgsign true

# == Rust ==
curl https://sh.rustup.rs -sSf | sh -s -- -y
source "$HOME"/.cargo/env
# Dev tools
rustup component add rust-analysis
rustup component add rls

# == tldr ==
cargo install tealdeer
tldr --update
# Fish completion
latest=$(wget -qO- https://api.github.com/repos/dbrgn/tealdeer/releases/latest)
url=$(grep 'browser_download_url": ".*/completions_fish"' <<< "$latest" | awk '{ print substr ($0, 32 ) }' | awk '{ print substr( $0, 1, length($0)-1 ) }')
wget -q --show-progress "$url" -O completions_fish
mv completions_fish ~/.config/fish/completions/tldr.fish

# == Git LFS ==
sudo pacman -S git-lfs --noconfirm

# == VSCode ==
yay -S visual-studio-code-bin --noconfirm

# == Spotify + no-ads mod ==
yay -S spotify --noconfirm
# Download mod
git clone https://github.com/abba23/spotify-adblock.git
cd spotify-adblock || exit
make
# Install mod
sudo make install
# Remove folder
cd ..
rm -rf spotify-adblock
# Change shortcut
if [ -f /usr/share/applications/spotify.desktop ] ; then
    sudo sed -i "7s#.*#Exec=env LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify %U#" /usr/share/applications/spotify.desktop
fi
# Remove banner
cd "$(dirname "$(realpath -L "$(which spotify)")")"/Apps/ || exit # absolute path to Spotify
sudo bash -c "unzip -p xpui.spa xpui.js | sed 's/{adsEnabled:\!0}/{adsEnabled:false}/' > xpui.js"
sudo zip --update xpui.spa xpui.js
sudo rm xpui.js

# == Syncthing ==
sudo pacman -S syncthing --noconfirm
# Launch it at startup
sudo systemctl enable syncthing@"$USER".service
sudo systemctl start syncthing@"$USER".service

# == Discord ==
sudo pacman -S discord --noconfirm

# == Obsidian ==
yay -S obsidian --noconfirm

# == Flameshot ==
sudo pacman -S flameshot xdg-desktop-portal xdg-desktop-portal-gnome --noconfirm
# Custom configuration
rm "$HOME"/.config/flameshot/flameshot.ini 2>/dev/null
wget -q --show-progress https://git.kennel.ml/Anri/manjaroConfiguration/raw/branch/main/.config/flameshot/flameshot.ini -P "$HOME"/.config/flameshot/
# Change keybindings
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot "[]"
CUSTOM_KEYBINDINGS_LIST=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
if [[ $CUSTOM_KEYBINDINGS_LIST == "@as []" ]] # creating new list if not exists
then
    CUSTOM_KEYBINDINGS_LIST="['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/']"
else # if already existing, adding our new element
    CUSTOM_KEYBINDINGS_LIST="${CUSTOM_KEYBINDINGS_LIST::-1}, '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/']"
fi
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "$CUSTOM_KEYBINDINGS_LIST"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/ name "Flameshot" # set name
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/ command "flameshot gui" # set command
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/ binding "Print" # set key

# == Parsec ==
yay -S parsec --noconfirm

# == Onlyoffice ==
sudo pacman -S onlyoffice --noconfirm

# Emoji picker and stome stuff
yay -S x11-emoji-picker-git --noconfirm

# Custom configuration
rm "$HOME"/.config/gazatu.xyz/emoji-picker.ini 2>/dev/null
wget -q --show-progress https://git.kennel.ml/Anri/manjaroConfiguration/raw/branch/main/.config/gazatu.xyz/emoji-picker.ini -P "$HOME"/.config/gazatu.xyz/
# Add keybinding
# Adding the keyboard shortcut
CUSTOM_KEYBINDINGS_LIST=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
if ! echo "$CUSTOM_KEYBINDINGS_LIST" | grep -q "x11-emoji-picker"
then
    if [ "$CUSTOM_KEYBINDINGS_LIST" = "@as []" ] # creating new list if not exists
        then
            CUSTOM_KEYBINDINGS_LIST="['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/x11-emoji-picker/']"
        else # if already existing, adding our new element
            CUSTOM_KEYBINDINGS_LIST=$(echo "$CUSTOM_KEYBINDINGS_LIST" | sed 's/\[//' | sed 's/\]//')
            CUSTOM_KEYBINDINGS_LIST="[$CUSTOM_KEYBINDINGS_LIST, '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/x11-emoji-picker/']"
    fi
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "$CUSTOM_KEYBINDINGS_LIST" # update the list
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/x11-emoji-picker/ name "Emoji-Picker" # set name
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/x11-emoji-picker/ command "emoji-picker" # set command
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/x11-emoji-picker/ binding "<Super>period" # set shortcut
fi

# == Gnome configuration ==
# Nautilus keybinding
CUSTOM_KEYBINDINGS_LIST=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
if [[ $CUSTOM_KEYBINDINGS_LIST == "@as []" ]] # creating new list if not exists
then
    CUSTOM_KEYBINDINGS_LIST="['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/']"
else # if already existing, adding our new element
    CUSTOM_KEYBINDINGS_LIST="${CUSTOM_KEYBINDINGS_LIST::-1}, '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/']"
fi
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "$CUSTOM_KEYBINDINGS_LIST" # updating the list
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/ name "Nautilus" # set name
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/ command "nautilus -w" # set command
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/ binding "<Super>E" # set shortcut
# Alt-F4 keybinding
gsettings set org.gnome.desktop.wm.keybindings close "['<Super>Q', '<Alt>F4']"
# Show battery poucentage
gsettings set org.gnome.desktop.interface show-battery-percentage true
# Remove sound of notification when going too far away in a window
gsettings set org.gnome.desktop.wm.preferences audible-bell false
# Add minimized window on dock click
gsettings set org.gnome.shell.extensions.dash-to-dock click-action minimize
# Add gesture
sudo systemctl enable touchegg.service
sudo systemctl start touchegg
# Better alt-tab
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Alt>Tab', '<Super>Tab']" # new behaviour
gsettings set org.gnome.desktop.wm.keybindings switch-applications "[]" # old behaviour
# Show icons on desktop
gsettings set org.gnome.desktop.background show-desktop-icons true
gsettings set org.gnome.shell.extensions.ding show-home false # hide personal user folder
gsettings set org.gnome.shell.extensions.ding show-drop-place false # hide square when moving icons
# Thumbnails nautilus
gsettings set org.gnome.nautilus.preferences show-image-thumbnails 'always'

# == Clipboard history ==
sudo pacman -S copyq --noconfirm
copyq > /dev/null & disown
copyq config autostart true

# == Signal ==
sudo pacman -S signal-desktop --noconfirm

# == Paint ==
sudo pacman -S kolourpaint --noconfirm
# Rename icon
if [ -f /usr/share/applications/org.kde.kolourpaint.desktop ] ; then
    sudo sed -i "3s#.*#Name=Paint#" /usr/share/applications/org.kde.kolourpaint.desktop
fi

# == Emojis ==
sudo pacman -S noto-fonts-emoji --noconfirm
sudo echo "<?xml version=\"1.0\"?>
<!DOCTYPE fontconfig SYSTEM \"fonts.dtd\">
<fontconfig>
 <alias>
   <family>sans-serif</family>
   <prefer>
     <family>Noto Sans</family>
     <family>Noto Color Emoji</family>
     <family>Noto Emoji</family>
     <family>DejaVu Sans</family>
   </prefer>
 </alias>

 <alias>
   <family>serif</family>
   <prefer>
     <family>Noto Serif</family>
     <family>Noto Color Emoji</family>
     <family>Noto Emoji</family>
     <family>DejaVu Serif</family>
   </prefer>
 </alias>

 <alias>
  <family>monospace</family>
  <prefer>
    <family>Noto Mono</family>
    <family>Noto Color Emoji</family>
    <family>Noto Emoji</family>
    <family>DejaVu Sans Mono</family>
   </prefer>
 </alias>
</fontconfig>
" | sudo tee /etc/fonts/local.conf
fc-cache

# == Power plan ==
sudo pacman -S tlp --noconfirm
sudo systemctl enable tlp.service
sudo systemctl mask systemd-rfkill.service
sudo systemctl mask systemd-rfkill.socket

# == Java ==
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
if [ -x "$HOME"/.config/fish ] ; then
   if [ ! -f "$HOME"/.config/fish/functions/fisher.fish ] ; then # install fisher if not already installed
      curl -sL https://git.io/fisher | fish -c 'source && fisher install jorgebucaran/fisher'
   fi
   fish -c 'fisher install reitzig/sdkman-for-fish'
fi
sdk install java 17.0.2-tem
echo "y" | sdk upgrade java

# == MultiMC ==
git clone https://github.com/MultiMC/multimc-pkgbuild.git
cd multimc-pkgbuild || exit
makepkg -si --noconfirm
cd ..
rm -rf multimc-pkgbuild/
yay -Y --gendb

echo -e "\nInstallation done.\nPlease restart your computer."
