# COLORED MANUALS
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

# SSH
fish_ssh_agent

# Exercism
set -gx PATH ~/bin $PATH
