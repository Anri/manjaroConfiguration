# Personal configuration for Manjaro

<details><summary>Détails concernant l'import/export de la clé GPG</summary>

  - Export clé publique : `gpg --output public.gpg --armor --export user@email`
  - Export clé privé : `gpg --output private.gpg --armor --export-secret-key user@email`
  - Import clés publique : `gpg --import public.gpg`
  - Import clés privé : `gpg --allow-secret-key-import --import private.gpg`
  - Fetch key number (`sec`) : `gpg --list-secret-keys --keyid-format LONG user@email` (dans `sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]` c'est `30F2B65B9246B6CA`)
  - Use this key for Git : `git config --global user.signingkey <numéro de la clé>`
</details>

Run this command to use my configuration (feel free to custom it btw) :
```bash
wget -q --show-progress https://git.kennel.ml/Anri/manjaroConfiguration/raw/branch/main/installation.sh -O tmp.sh && bash tmp.sh; rm tmp.sh
```
